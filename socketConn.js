// import socketIOClient from 'socket.io-client';
// module.exports = async (taskData) => {
//   const ENDPOINT = 'http://192.168.1.4:9000/';
//   const socket = socketIOClient(ENDPOINT, {
//     transports: ['websocket'],
//     upgrade: false,
//   });
//   console.log('socket running');


//   setInterval(() => {
//       console.log('This will run every second! socket');
//     }, 1000);

// };

import BackgroundJob from 'react-native-background-actions';

const sleep = time => new Promise(resolve => setTimeout(() => resolve(), time));

const taskRandom = async taskData => {
  if (Platform.OS === 'ios') {
    console.warn(
      'This task will not keep your app alive in the background by itself, use other library like react-native-track-player that use audio,',
      'geolocalization, etc. to keep your app alive in the background while you excute the JS from this library.',
    );
  }
  await new Promise(async resolve => {
    // For loop with a delay
    const {delay} = taskData;
    for (let i = 0; BackgroundJob.isRunning(); i++) {
      console.log('Runned -> ', i);
      await BackgroundJob.updateNotification({taskDesc: 'Running -> ' + i});
      await sleep(delay);
    }
  });
};

const options = {
  taskName: 'Example',
  taskTitle: 'ExampleTask title',
  taskDesc: 'ExampleTask desc',
  taskIcon: {
    name: 'ic_launcher',
    type: 'mipmap',
  },
  color: '#ff00ff',
  linkingURI: 'exampleScheme://chat/jane',
  parameters: {
    delay: 1000,
  },
};


module.exports = async (taskData) => {
console.log('Trying to start background service');
await BackgroundJob.start(taskRandom, options);
console.log('Successful start!');
}